package com.mongo.Controller;

/**
 * 
 */

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

public class TestAnswer {

	private String id;
	private String questionId;
	private String userGivenQuestionAnswer;
	private String correctQuestionAnswer;
	private String result;
	private String type;
	private String audioFileName;
	private String categoryName;
	private String weightage;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public String getUserGivenQuestionAnswer() {
		return userGivenQuestionAnswer;
	}

	public void setUserGivenQuestionAnswer(String userGivenQuestionAnswer) {
		this.userGivenQuestionAnswer = userGivenQuestionAnswer;
	}

	public String getCorrectQuestionAnswer() {
		return correctQuestionAnswer;
	}

	public void setCorrectQuestionAnswer(String correctQuestionAnswer) {
		this.correctQuestionAnswer = correctQuestionAnswer;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAudioFileName() {
		return audioFileName;
	}

	public void setAudioFileName(String audioFileName) {
		this.audioFileName = audioFileName;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getWeightage() {
		return weightage;
	}

	public void setWeightage(String weightage) {
		this.weightage = weightage;
	}

	@Override
	public String toString() {
		return "TestAnswer [id=" + id + ", questionId=" + questionId + ", userGivenQuestionAnswer="
				+ userGivenQuestionAnswer + ", correctQuestionAnswer=" + correctQuestionAnswer + ", result=" + result
				+ ", type=" + type + ", audioFileName=" + audioFileName + ", categoryName=" + categoryName
				+ ", weightage=" + weightage + "]";
	}

}
