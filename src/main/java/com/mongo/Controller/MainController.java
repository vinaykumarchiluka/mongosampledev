package com.mongo.Controller;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.limit;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.skip;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.util.CloseableIterator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mongo.Model.Category;
import com.mongo.Model.CategoryTO;
import com.mongo.Model.Question;
import com.mongodb.Cursor;
import com.mongodb.client.FindIterable;

@Controller
public class MainController {

	@Autowired
	private MongoTemplate mongoTemplate;

	@RequestMapping(value = "/home")
	@ResponseBody
	public String mongocheck() {
		System.out.println("hello");

	/*	Aggregation agg = newAggregation(match(Criteria.where("_id").is("57760256e4b00b0a86499876")))
				.withOptions(AggregationOptions.builder().cursor(new Document()).build());

		AggregationResults<Question> groupResults = mongoTemplate.aggregate(agg, Question.class, Question.class).allowDiskUse(options.isAllowDiskUse()) //
		        .useCursor(true);;

		List<Question> questionList = groupResults.getMappedResults();
		*/
		
		
		
		
		
		/*
	db.category.aggregate([
   { $match: {
      "companyId" : "707"
  } },
  { $lookup: {
            "from": "question", 
            "localField": "categoryName", 
            "foreignField": "category.categoryName", 
            "as": "question", 
  } },
    {   $unwind:"$question" },
 {
    "$project": {
      "question._id": {
        "$toString": "$question._id"
      },
      "question.question": "$question.question"
    }
  },  

  
 
  { $lookup: {
            "from": "testAnswersInformation", 
            "localField": "question._id", 
            "foreignField": "testAnswers.questionId", 
            "as": "testAnswersInformation", 
  } },
 
  
])
  */
		
		AggregationOptions aggregationOptions = Aggregation.newAggregationOptions().cursor(new Document())
			    .build();
		
		LookupOperation lookupOperation = LookupOperation.newLookup()
			    .from("question")
			    .localField("categoryName")
			    .foreignField("category.categoryName")
			    .as("question");
		
		LookupOperation lookupOperation2 = LookupOperation.newLookup()
			    .from("testAnswersInformation")
			    .localField("question._id")
			    .foreignField("testAnswers.questionId")
			    .as("testAnswersInformation");
		ProjectionOperation projectToMatchModel = project()
				  .andExpression("question._id.toString()","_id").as("question");
			  
		Aggregation agg = newAggregation(
				Aggregation.match(Criteria.where("companyId").is("330")),
				lookupOperation/*,
			    unwind("question"),
			    projectToMatchModel,
			    lookupOperation2  */
			);
		
		
		CloseableIterator<CategoryTO> groupResults =  mongoTemplate.aggregateStream(agg.withOptions(aggregationOptions), Category.class, CategoryTO.class);
		List<CategoryTO> myList = new ArrayList<CategoryTO>();   
		
		while (groupResults.hasNext()) {
			myList.add(groupResults.next());
		//	System.out.println("size---->>"+groupResults.next().getQuestion().toString());
			
			
		}
		System.out.println("size----------->>>>>"+myList.size());

		return "check";
	}

	
}
