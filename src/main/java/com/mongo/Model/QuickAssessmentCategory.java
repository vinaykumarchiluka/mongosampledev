package com.mongo.Model;



import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Shashidhara ML
 * 
 */
@Document(collection = "QuickAssessmentCategory")
public class QuickAssessmentCategory {
	@Id
	private String categoryId;
	private String categoryName;
	private int noOfQuestions;
	private String parentCategoryId;
	private String categoryLabel;
	private int easyQuestionsCount;
	private int mediumQuestionsCount;
	private int hardQuestionsCount;

	/**
	 * @return the categoryId
	 */
	public String getCategoryId() {
		return categoryId;
	}

	/**
	 * @param categoryId
	 *            the categoryId to set
	 */
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 * @param categoryName
	 *            the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/**
	 * @return the noOfQuestions
	 */
	public int getNoOfQuestions() {
		return noOfQuestions;
	}

	/**
	 * @param noOfQuestions
	 *            the noOfQuestions to set
	 */
	public void setNoOfQuestions(int noOfQuestions) {
		this.noOfQuestions = noOfQuestions;
	}

	/**
	 * @return the parentCategoryId
	 */
	public String getParentCategoryId() {
		return parentCategoryId;
	}

	/**
	 * @param parentCategoryId
	 *            the parentCategoryId to set
	 */
	public void setParentCategoryId(String parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}

	/**
	 * @return the categoryLabel
	 */
	public String getCategoryLabel() {
		return categoryLabel;
	}

	/**
	 * @param categoryLabel
	 *            the categoryLabel to set
	 */
	public void setCategoryLabel(String categoryLabel) {
		this.categoryLabel = categoryLabel;
	}

	/**
	 * @return the easyQuestionsCount
	 */
	public int getEasyQuestionsCount() {
		return easyQuestionsCount;
	}

	/**
	 * @param easyQuestionsCount
	 *            the easyQuestionsCount to set
	 */
	public void setEasyQuestionsCount(int easyQuestionsCount) {
		this.easyQuestionsCount = easyQuestionsCount;
	}

	/**
	 * @return the mediumQuestionsCount
	 */
	public int getMediumQuestionsCount() {
		return mediumQuestionsCount;
	}

	/**
	 * @param mediumQuestionsCount
	 *            the mediumQuestionsCount to set
	 */
	public void setMediumQuestionsCount(int mediumQuestionsCount) {
		this.mediumQuestionsCount = mediumQuestionsCount;
	}

	/**
	 * @return the hardQuestionsCount
	 */
	public int getHardQuestionsCount() {
		return hardQuestionsCount;
	}

	/**
	 * @param hardQuestionsCount
	 *            the hardQuestionsCount to set
	 */
	public void setHardQuestionsCount(int hardQuestionsCount) {
		this.hardQuestionsCount = hardQuestionsCount;
	}

}

