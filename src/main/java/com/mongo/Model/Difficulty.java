package com.mongo.Model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "difficulty")
public class Difficulty {
	
	private String id;
	private String difficultyLevel;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDifficultyLevel() {
		return difficultyLevel;
	}

	public void setDifficultyLevel(String difficultyLevel) {
		this.difficultyLevel = difficultyLevel;
	}

}

