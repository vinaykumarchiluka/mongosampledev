package com.mongo.Model;


/**
 * 
 */

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Shashidhara ML
 * 
 */
public class CategoryTO {
	
	private String id;
	private String categoryName;
	private String parentId;
	private String noOfQuestions;
	private String catagoryLabel;
	private String companyId;
	private String recTestId;
	private String applicationName;
	private Integer sequenceNumber;
	private String userId;
	private String compQuestionIds;
	private String jobRoleId;
	private String languageType; 
	private Question question;
	//private TestAnswersInformation testAnswersInformation;

	public String getNoOfQuestions() {
		return noOfQuestions;
	}

	public void setNoOfQuestions(String noOfQuestions) {
		this.noOfQuestions = noOfQuestions;
	}

	public String getCatagoryLabel() {
		return catagoryLabel;
	}

	public void setCatagoryLabel(String catagoryLabel) {
		this.catagoryLabel = catagoryLabel;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 * @param categoryName
	 *            the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/**
	 * @return the parentId
	 */
	public String getParentId() {
		return parentId;
	}

	/**
	 * @param parentId
	 *            the parentId to set
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getRecTestId() {
		return recTestId;
	}

	public void setRecTestId(String recTestId) {
		this.recTestId = recTestId;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public Integer getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the compQuestionIds
	 */
	public String getCompQuestionIds() {
		return compQuestionIds;
	}

	/**
	 * @param compQuestionIds the compQuestionIds to set
	 */
	public void setCompQuestionIds(String compQuestionIds) {
		this.compQuestionIds = compQuestionIds;
	}

	public String getJobRoleId() {
		return jobRoleId;
	}

	public void setJobRoleId(String jobRoleId) {
		this.jobRoleId = jobRoleId;
	}

	public String getLanguageType() {
		return languageType;
	}

	public void setLanguageType(String languageType) {
		this.languageType = languageType;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	@Override
	public String toString() {
		return "CategoryTO [categoryName=" + categoryName + ", question=" + question + "]";
	}


	

	
	
	
}


