package com.mongo.Model;




import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.mongo.Controller.TestAnswer;

/**
 * @author Shashidhara ML
 * 
 */
@Document(collection = "testAnswersInformation")
public class TestAnswersInformation {

	@Id
	private String id;
	private String testId;
	private String candidateId;
	private String testTakenDate;
	private String testScore;
	private String result;
	private String totalWeightage;
	private String correctAnswers;
	private String errorMessage;
	private List<TestAnswer> testAnswers;
	private String jrfId;
	private String correctScore;
	private String type;
	private String timerCountString;
	private String submitType;
	private String definedTime;
	private String userTotalTestTime;
	private String autotestSubmitDiff;
	private String remarks;
	private String suspisiousCount;
	private String photoCount;
	private String negativemarksScore;
	private boolean negativeMarking;
	private Date testDoneDate;
	

	public Date getTestDoneDate() {
		return testDoneDate;
	}

	public void setTestDoneDate(Date testDoneDate) {
		this.testDoneDate = testDoneDate;
	}

	public String getSuspisiousCount() {
		return suspisiousCount;
	}

	public void setSuspisiousCount(String suspisiousCount) {
		this.suspisiousCount = suspisiousCount;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the testId
	 */
	public String getTestId() {
		return testId;
	}

	/**
	 * @param testId
	 *            the testId to set
	 */
	public void setTestId(String testId) {
		this.testId = testId;
	}

	/**
	 * @return the candidateId
	 */
	public String getCandidateId() {
		return candidateId;
	}

	/**
	 * @param candidateId
	 *            the candidateId to set
	 */
	public void setCandidateId(String candidateId) {
		this.candidateId = candidateId;
	}

	/**
	 * @return the testTakenDate
	 */
	public String getTestTakenDate() {
		return testTakenDate;
	}

	/**
	 * @param testTakenDate
	 *            the testTakenDate to set
	 */
	public void setTestTakenDate(String testTakenDate) {
		this.testTakenDate = testTakenDate;
	}

	/**
	 * @return the testScore
	 */
	public String getTestScore() {
		return testScore;
	}

	/**
	 * @param testScore
	 *            the testScore to set
	 */
	public void setTestScore(String testScore) {
		this.testScore = testScore;
	}

	/**
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @param result
	 *            the result to set
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * @return the totalWeightage
	 */
	public String getTotalWeightage() {
		return totalWeightage;
	}

	/**
	 * @param totalWeightage
	 *            the totalWeightage to set
	 */
	public void setTotalWeightage(String totalWeightage) {
		this.totalWeightage = totalWeightage;
	}

	/**
	 * @return the correctAnswers
	 */
	public String getCorrectAnswers() {
		return correctAnswers;
	}

	/**
	 * @param correctAnswers
	 *            the correctAnswers to set
	 */
	public void setCorrectAnswers(String correctAnswers) {
		this.correctAnswers = correctAnswers;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage
	 *            the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the testAnswers
	 */
	public List<TestAnswer> getTestAnswers() {
		return testAnswers;
	}

	/**
	 * @param testAnswers
	 *            the testAnswers to set
	 */
	public void setTestAnswers(List<TestAnswer> testAnswers) {
		this.testAnswers = testAnswers;
	}

	/**
	 * @return the jrfId
	 */
	public String getJrfId() {
		return jrfId;
	}

	/**
	 * @param jrfId
	 *            the jrfId to set
	 */
	public void setJrfId(String jrfId) {
		this.jrfId = jrfId;
	}

	/**
	 * @return the correctScore
	 */
	public String getCorrectScore() {
		return correctScore;
	}

	/**
	 * @param correctScore
	 *            the correctScore to set
	 */
	public void setCorrectScore(String correctScore) {
		this.correctScore = correctScore;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTimerCountString() {
		return timerCountString;
	}

	public void setTimerCountString(String timerCountString) {
		this.timerCountString = timerCountString;
	}

	public String getSubmitType() {
		return submitType;
	}

	public void setSubmitType(String submitType) {
		this.submitType = submitType;
	}

	public String getUserTotalTestTime() {
		return userTotalTestTime;
	}

	public void setUserTotalTestTime(String userTotalTestTime) {
		this.userTotalTestTime = userTotalTestTime;
	}

	public String getAutotestSubmitDiff() {
		return autotestSubmitDiff;
	}

	public void setAutotestSubmitDiff(String autotestSubmitDiff) {
		this.autotestSubmitDiff = autotestSubmitDiff;
	}

	public String getDefinedTime() {
		return definedTime;
	}

	public void setDefinedTime(String definedTime) {
		this.definedTime = definedTime;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getPhotoCount() {
		return photoCount;
	}

	public void setPhotoCount(String photoCount) {
		this.photoCount = photoCount;
	}

	/**
	 * @return the negativemarksScore
	 */
	public String getNegativemarksScore() {
		return negativemarksScore;
	}

	/**
	 * @param negativemarksScore
	 *            the negativemarksScore to set
	 */
	public void setNegativemarksScore(String negativemarksScore) {
		this.negativemarksScore = negativemarksScore;
	}

	/**
	 * @return the negativeMarking
	 */
	public boolean isNegativeMarking() {
		return negativeMarking;
	}

	/**
	 * @param negativeMarking
	 *            the negativeMarking to set
	 */
	public void setNegativeMarking(boolean negativeMarking) {
		this.negativeMarking = negativeMarking;
	}

	@Override
	public String toString() {
		return "TestAnswersInformation [testId=" + testId + ", testTakenDate=" + testTakenDate + ", correctAnswers="
				+ correctAnswers + ", testAnswers=" + testAnswers + "]";
	}

}

