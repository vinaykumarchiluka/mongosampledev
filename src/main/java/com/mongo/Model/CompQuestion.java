package com.mongo.Model;


import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "CompQuestion")
public class CompQuestion {
	@org.springframework.data.annotation.Id
	private String id;
	private String questionId;
	private String parentQuesId;
	private Integer childQuesCount;
	private String comprehensionquestion;
	private String parentQuestion;
	private CompQuestionOption option;
	private String answer;
	private String weightage;
	private boolean isMultiAnswerQuestion;
	private List<String> assessts;
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWeightage() {
		return weightage;
	}

	public void setWeightage(String weightage) {
		this.weightage = weightage;
	}

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public CompQuestionOption getOption() {
		return option;
	}

	public void setOption(CompQuestionOption option) {
		this.option = option;
	}

	public String getParentQuestion() {
		return parentQuestion;
	}

	public void setParentQuestion(String parentQuestion) {
		this.parentQuestion = parentQuestion;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getComprehensionquestion() {
		return comprehensionquestion;
	}

	public void setComprehensionquestion(String comprehensionquestion) {
		this.comprehensionquestion = comprehensionquestion;
	}

	public Integer getChildQuesCount() {
		return childQuesCount;
	}

	public void setChildQuesCount(Integer childQuesCount) {
		this.childQuesCount = childQuesCount;
	}

	public String getParentQuesId() {
		return parentQuesId;
	}

	public void setParentQuesId(String parentQuesId) {
		this.parentQuesId = parentQuesId;
	}

	/**
	 * @return the isMultiAnswerQuestion
	 */
	public boolean isMultiAnswerQuestion() {
		return isMultiAnswerQuestion;
	}

	/**
	 * @param isMultiAnswerQuestion
	 *            the isMultiAnswerQuestion to set
	 */
	public void setMultiAnswerQuestion(boolean isMultiAnswerQuestion) {
		this.isMultiAnswerQuestion = isMultiAnswerQuestion;
	}
	public List<String> getAssessts() {
		return assessts;
	}

	public void setAssessts(List<String> assessts) {
		this.assessts = assessts;
	}
}

