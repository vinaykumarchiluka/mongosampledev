package com.mongo.Model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Shashidhara ML
 * 
 */
@Document(collection = "QuickAssessmentRule")
public class QuickAssessmentRule {
	@Id
	private String id;
	private String testName;
	private String description;
	private String applicationName;
	private String recTestId;
	private String assessmentDuration;
	private String assessmentCutOff;
	private List<QuickAssessmentCategory> quickAssessmentCategory;
	private String companyId;
	private String userId;
    private String status;
    private String createdDate;
    private String jobRoleId;
	private String language;
    
	

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the testName
	 */
	public String getTestName() {
		return testName;
	}

	/**
	 * @param testName
	 *            the testName to set
	 */
	public void setTestName(String testName) {
		this.testName = testName;
	}

	/**
	 * @return the applicationName
	 */
	public String getApplicationName() {
		return applicationName;
	}

	/**
	 * @param applicationName
	 *            the applicationName to set
	 */
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	/**
	 * @return the recTestId
	 */
	public String getRecTestId() {
		return recTestId;
	}

	/**
	 * @param recTestId
	 *            the recTestId to set
	 */
	public void setRecTestId(String recTestId) {
		this.recTestId = recTestId;
	}

	/**
	 * @return the quickAssessmentCategory
	 */
	public List<QuickAssessmentCategory> getQuickAssessmentCategory() {
		return quickAssessmentCategory;
	}

	/**
	 * @param quickAssessmentCategory
	 *            the quickAssessmentCategory to set
	 */
	public void setQuickAssessmentCategory(
			List<QuickAssessmentCategory> quickAssessmentCategory) {
		this.quickAssessmentCategory = quickAssessmentCategory;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public String getAssessmentDuration() {
		return assessmentDuration;
	}

	public void setAssessmentDuration(String assessmentDuration) {
		this.assessmentDuration = assessmentDuration;
	}

	public String getAssessmentCutOff() {
		return assessmentCutOff;
	}

	public void setAssessmentCutOff(String assessmentCutOff) {
		this.assessmentCutOff = assessmentCutOff;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getJobRoleId() {
		return jobRoleId;
	}

	public void setJobRoleId(String jobRoleId) {
		this.jobRoleId = jobRoleId;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
}
